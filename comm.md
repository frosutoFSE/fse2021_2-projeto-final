# Comunicação JSON

## topic

- general_prefix: `fse2021/<matricula>`
- config_topic: `fse2021/<matricula>/dispositivos/<id_dispositivo>`
- room_topic: `fse2021/<matricula>/<comodo>/...`

### Json General

todas as mensagens têm esses atributos
```json
{
    "sender": "esp32" | "central",
}
```

### Pedido de conexão da esp

`fse2021/<matricula>/dispositivos/<id_dispositivo>`

```json
{
    "sender": "esp32",
    "config": "conexao",
    "esp_type": "energia" | "bateria"
}
```

### Retorno de configuração do servidor

`fse2021/<matricula>/dispositivos/<id_dispositivo>`

se esp for do tipo `energia`

```json
{
    "sender": "central",
    "config": "conexao",
    "comodo": "<nome do comodo>",
    "dimerizavel": 1 | 0,
    "input": {
        "dispositivo": "<nome do dispositivo, e.g. sensor-porta",
    },
    "output": {
        "dispositivo": "nome do dispositivo, e.g. lampada"
    }
}
```

### Desconexão vinda de qualquer lado

`fse2021/<matricula>/dispositivos/<id_dispositivo>`

O servidor retira o a esp do cadastro e se desinscreve dos tópicos do cômodo

A esp deve voltar para o "modo de fábrica"

```json
{
    "sender": "esp32" | "central",
    "config": "desconexao",
}
```

### Envio de temperatura

`fse2021/<matricula>/<comodo>/temperatura`

```json
{
    "sender": "esp32",
    "dado": float
}
```

### Envio de umidade

`fse2021/<matricula>/<comodo>/umidade`

```json
{
    "sender": "esp32",
    "dado": float
}
```

### Envio do estado da entrada

`fse2021/<matricula>/<comodo>/estado`

```json
{
    "sender": "esp32",
    "input": {
        "dispositivo": "nome do dispositivo",
        "estado": 1 | 0
    }
}
```

### Envio do estado de saída binário

`fse2021/<matricula>/<comodo>/estado`

```json
{
    "sender": "esp32" | "central",
    "output": {
        "dispositivo": "nome do dispositivo",
        "estado": 1 | 0
    } 
}
```

### Envio do estado de saída dimerizável

`fse2021/<matricula>/<comodo>/estado`

```json
{
    "sender": "esp32" | "central",
    "output": {
        "dispositivo": "nome do dispositivo",
        "estado": int // range de 0 a 100 (%)
    }
}
```

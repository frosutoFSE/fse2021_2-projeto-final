import paho.mqtt.client as mqtt
import json
from time import sleep
import random

client = mqtt.Client()

client.connect("test.mosquitto.org", 1883, 60)

i = 0
while True:
    try:
        if i==0:
            client.publish(f"fse2021/180029177/dispositivos/{str(i)}", json.dumps({"id": i,"sender": "esp32","config": "conexao"}))
        else:
            print()
            client.publish("fse2021/180029177/Sala/temperatura", json.dumps({"sender": "esp32", "dado": random.randint(20, 35)}))
            client.publish("fse2021/180029177/Sala/umidade", json.dumps({"sender": "esp32", "dado": random.randint(0, 100)}))
            client.publish("fse2021/180029177/Sala/situacao", json.dumps({"sender": "esp32", "dado": 1}))

        sleep(10)
        i+=1

        if i==10:
            i=0
    except KeyboardInterrupt:
        print("Programa finalizado")
        break
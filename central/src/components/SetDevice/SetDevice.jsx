import { useState } from "react";
import { Button, Modal, Form } from "react-bootstrap";

function SetDevice(props) {
  const [comodo, setComodo] = useState("");
  const [nomeEntrada, setNomeEntrada] = useState("");
  const [acionaAlarme, setAcionaAlarme] = useState(false);
  const [nomeSaida, setNomeSaida] = useState("");
  const [saidaDimerizavel, setSaidaDimerizavel] = useState(false);

  const handleSubmit = (event) => {
    const data = {
      "id": props.currentDevice.id,
      "comodo": comodo,
      "nomeEntrada": nomeEntrada,
      "nomeSaida": nomeSaida,
      "saidaDimerizavel": saidaDimerizavel,
      "acionaAlarme": acionaAlarme,
    };
    props.parentCallback(data);
    props.onHide();
    event.preventDefault();
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          Configurando dispositivo
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h2>ID: {props.currentDevice.id}</h2>
        <Form>
          <Form.Group className="mb-3" controlId="comodo">
            <Form.Label>Comodo do dispositivo</Form.Label>{" "}
            <Form.Control
              onChange={(e) => {setComodo(e.target.value)}}
              type="text"
              placeholder="Sala"
              autoFocus
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="entrada">
            <Form.Label>Nome da entrada</Form.Label>
            <Form.Control
              onChange={(e) => {setNomeEntrada(e.target.value)}}
              type="text"
              placeholder="Lâmpada"
            />
            <Form.Check
              onChange={() => {
                setAcionaAlarme((prevState) => !prevState);
              }}
              type="switch"
              label="Aciona alarme"
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="saida">
            <Form.Label>Nome da saída</Form.Label>
            <Form.Control
              onChange={(e) => {setNomeSaida(e.target.value)}}
              type="text"
              placeholder="Força da luz da lâmpada"
            />
            <Form.Check
              onChange={() => {
                setSaidaDimerizavel((prevState) => !prevState);
              }}
              type="switch"
              label="Saída dimerizável"
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleSubmit}>Enviar</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default SetDevice;
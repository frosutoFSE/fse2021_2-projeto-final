import { Form, Dropdown, DropdownButton } from "react-bootstrap";
import { useEffect, useState } from "react";
import "./Esp32.css";

import sound from "../../audio/alarm.mp3";

function Esp32(props) {
  const [alarme, setAlarme] = useState(false);
  const [situacao, setSituacao] = useState(true);

  const [valorDimerizavel, setValorDimerizavel] = useState(1);

  const [temperatura, setTemperatura] = useState(false);
  const [umidade, setUmidade] = useState(false);

  useEffect(() => {
    props.client.on("message", (topic, message) => {
      //Se increve nas informações recebidas do comodo
      const data = JSON.parse(message);

      if (topic === `fse2021/180029177/${props.data.comodo}/temperatura`) {
        setTemperatura(parseFloat(data.dado));
      } else if (topic === `fse2021/180029177/${props.data.comodo}/umidade`) {
        setUmidade(parseFloat(data.dado));
      } else if (topic === `fse2021/180029177/${props.data.comodo}/estado`) {
        if (data.sender === "esp32") {
          setSituacao(parseInt(data.situacao) === 1 ? true : false);
        }
        if (alarme) {
          if (parseInt(data.input.estado) === 1) {
            const audio = new Audio(sound);
            audio.play();
          }
          setAlarme(false);
        }
      }
    });
  }, [props, situacao, temperatura, umidade, alarme]);

  const handleDimerizavel = (e) => {
    e.preventDefault();
    setValorDimerizavel(parseInt(e.target.innerText));

    const data = {
      sender: "central",
      output: {
        dispositivo: props.data.nomeSaida,
        estado: parseInt(e.target.innerText),
      },
    };

    props.client.publish(
      `fse2021/180029177/${props.data.comodo}/estado`,
      JSON.stringify(data),
      { qos: 1 }
    );
  };

  const handleSituacao = () => {
    if (alarme) {
      const audio = new Audio(sound);
      audio.play();
      setAlarme(false);
    }

    const data = {
      sender: "central",
      output: {
        dispositivo: props.data.nomeSaida,
        estado: situacao ? 1 : 0,
      },
    };

    props.client.publish(
      `fse2021/180029177/${props.data.comodo}/estado`,
      JSON.stringify(data),
      { qos: 1 }
    );
  };

  const handleAlarme = () => {
    setAlarme(!alarme);
  };

  return (
    <div className="Esp32">
      <h3>{`Esp ${props.data.id}`}</h3>
      <div className="esp32_info">
        <div className="comodo">
          <h4>{props.data.comodo}</h4>
        </div>
        {/* <div className="status">
          <h4>Conectado</h4>
        </div> */}
      </div>
      <div className="esp32_info">
        <div className="temperatura">
          {temperatura ? (
            <h4>{`${Math.round(temperatura)}ºC`}</h4>
          ) : (
            <h4>-ºC</h4>
          )}
        </div>
        <div className="umidade">
          {umidade ? <h4>{`${Math.round(umidade)}%`}</h4> : <h4>-%</h4>}
        </div>
      </div>
      <div className="interacao">
        {props.data.saidaDimerizavel ? (
          <div className="dimerizavel">
            <Form
              onSubmit={(e) => {
                e.preventDefault();
              }}
            >
              <Form.Group
                className="mb-3"
                controlId="exampleForm.valorDimerizavel"
              >
                <DropdownButton
                  id="dropdown-variant"
                  variants="Success"
                  title="Valor Dimerizável"
                >
                  {[...Array(100)].map((_, i) => (
                    <Dropdown.Item
                      key={i}
                      onClick={(e) => handleDimerizavel(e)}
                    >
                      {i}
                    </Dropdown.Item>
                  ))}
                </DropdownButton>
              </Form.Group>
            </Form>
          </div>
        ) : situacao ? (
          <button className="btn btn-success" onClick={handleSituacao}>
            Ligar saída
          </button>
        ) : (
          <button className="btn btn-danger" onClick={handleSituacao}>
            Desligar saída
          </button>
        )}
      </div>
      {props.data.acionaAlarme ? (
        alarme ? (
          <div className="interacao">
            <button className="btn btn-danger" onClick={handleAlarme}>
              Desligar alarme
            </button>
          </div>
        ) : (
          <div className="interacao">
            <button className="btn btn-success" onClick={handleAlarme}>
              Ligar alarme
            </button>
          </div>
        )
      ) : (
        <div className="interacao">
          <h6>Não há acionamento de alarme</h6>
        </div>
      )}
      <div className="interacao">
        <button
          className="btn btn-danger"
          onClick={() => {
            props.parentCallbackRemove(props.data.id);
          }}
        >
          Remover dispositivo
        </button>
      </div>
    </div>
  );
}

export default Esp32;

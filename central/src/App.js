import { useState, useEffect } from "react";

import "./App.css";

import Esp32 from "./components/Esp32/Esp32";
import SetDevice from "./components/SetDevice/SetDevice";

const mqtt = require("mqtt/dist/mqtt.js");

const broker = "mqtt://test.mosquitto.org:8081";

const client = mqtt.connect(broker);

function App() {
  const [connectStatus, setConnectStatus] = useState(false);
  const [newDevice, setNewDevice] = useState(false);
  const [devices, setDevices] = useState([]);
  const [currentDevice, setCurrentDevice] = useState([]);

  useEffect(() => {
    client.on("connect", () => {
      setConnectStatus(true);
      client.subscribe("fse2021/180029177/dispositivos/#");
    });

    client.on("message", (topic, message) => {
      const id = topic.split("/")[3];
      const data = JSON.parse(message);
      data["id"] = id;
      console.log(`Mensagem recebida do tópico ${topic}: ${data.id}`);
      console.log(data);
      if (data.sender === "esp32" && data.config === "conexao") {
        setNewDevice(true);
        setCurrentDevice(data);
      }
    });
  }, [devices]);

  const handleCallback = (childData) => {
    setDevices((prevState) => [...prevState, childData]);

    client.subscribe(`fse2021/180029177/${childData.comodo}/temperatura`);
    client.subscribe(`fse2021/180029177/${childData.comodo}/umidade`);
    client.subscribe(`fse2021/180029177/${childData.comodo}/estado`);

    if (connectStatus) {
      // Envia as informações de configuração
      const data = {
        sender: "central",
        config: "conexao",
        comodo: childData.comodo,
        dimerizavel: childData.saidaDimerizavel ? 1 : 0,
        alarme: childData.acionaAlarme ? 1 : 0, // Adicionar no comm.md
        input: {
          dispositivo: childData.nomeEntrada,
        },
        output: {
          dispositivo: childData.nomeSaida,
        },
      };

      client.publish(
        `fse2021/180029177/dispositivos/${childData.id}`,
        JSON.stringify(data),
        { qos: 1 }
      );
    }
  };

  const handleRemove = (id) => {
    setDevices((prevState) => prevState.filter((device) => device.id !== id));

    // Enviar mensagem de desconexão
    const data = {
      sender: "central",
      config: "desconexao",
    };

    client.publish(
      `fse2021/180029177/dispositivos/${id}`,
      JSON.stringify(data),
      { qos: 1 }
    );
  };

  return (
    <div className="App">
      <div className="App-header">
        <h2>{connectStatus ? "Conectado ao MQTT" : "Desconectado do MQTT"}</h2>
      </div>
      <div className="clientes">
        <h1>Clientes</h1>
        {devices.map((item, index) => (
          <Esp32
            client={client}
            key={index}
            data={item}
            parentCallbackRemove={handleRemove}
          />
        ))}
      </div>
      <SetDevice
        show={newDevice}
        currentDevice={currentDevice}
        parentCallback={handleCallback}
        onHide={() => setNewDevice(false)}
      />
    </div>
  );
}

export default App;

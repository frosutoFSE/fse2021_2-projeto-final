# FSE 2021.2 - Projeto Final

Repositório para o projeto final de Fundamentos de Sistemas Embarcados de 2021.2

## Integrantes

|Matrícula|Discente|
|--|--|
|18/0102656|Ítalo Vinícius Pereira Guimarães|
|18/0029177|Wagner Martins da Cunha|

## Sobre

O objetivo deste trabalho é criar um sistema de automação residencial utilizando um computador como servidor central e microcontroladores esp32 como dispositivos distribuídos, interconectados atráves do protocolo MQTT. Mais informações, incluindo os requisitos do projeto podem ser encontrados no repositório [Trabalho Final 2021/2](https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-final-2021-2).

## Apresentação

O vídeo de apresentação pode ser visto no [youtube](https://youtu.be/Yyv5g22uE58).

## Rodando o Sistema

### Servidor Central

#### Requisitos

- Ter o docker-compose instalado em sua máquina

#### Passos

Acesse a pasta "central"

```sh
cd central/
```

Execute o comando abaixo:

```sh
docker-compose up --build
```

Espere o build ser feito e o programa estar executando, após isso basta acessar este <a href="http://localhost:3000/">link</a> que seria a porta local 3000 e esperar as interações da esp32 para que possar fazer a conexão e as possíveis interações.

### Servidores Distribuídos (esp32)

A versão da esp-idf utilizada é a latest no final de abril 2022, mais especificamente a versão `ESP-IDF v5.0-dev-2393-gfaed2a44aa`, obtida utilizando o comando `idf.py --version`.

Os comandos abaixo devem ser feitos no diretório [cliente_esp32](./cliente_esp32) e consideram que o ambiente idf está configurado na máquina. O tutorial de instalação da esp-idf pode ser encontrada no [site](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html).

#### Menuconfig

Algumas informações devem ser setadas para que a aplicação na esp32 rode corretamente. Abra o menuconfig com o comando

```sh
idf.py menuconfig
```

No menu `Serial flasher config`, atualize o valor de `Flash size` para a quantidade de memória flash da esp (normalmente 4 MB). Depois vá ao menu `Partition table`, configure para `Custom partition table CSV` e mude o nome em `Custom partition CSV file` para [partitions.csv](./cliente_esp32/partitions.csv).

Logo em seguida, a partir do menu inicial, vá para o menu `Configuração do projeto`. A seguir uma breve descrição das opções disponíveis:

|Nome da Opção|Descrição|Nota|
|-------------|---------|----|
|Tipo de Dispositivo|O tipo da ESP|Manter como `Energia`. A esp Bateria não foi implementada e por isso não está funcional|
|Matricula utilizada nos tópicos|Matrícula utilizada nos tópicos|Para rodar com o servidor central, manter a Default.
|WiFi SSID|Nome da Rede Wifi||
|Senha do Wifi| A senha da Rede Wifi||
|Número máximo de tentativas de conexão|Número máximo de tentativas de conexão|A esp reiniciará se estourar este número|
|Endereço para o broker MQTT|Endereço IP do Broker MQTT||
|Porta de acesso do broker MQTT|Porta do Broker MQTT||

#### Build

Para buildar a aplicação, utilize o comando

```sh
idf.py build
```

#### Executando na ESP32

Com a placa conectada ao computador, no diretório [cliente_esp32](./cliente_esp32), utilize o comando

```sh
idf.py -p /dev/ttyUSB0 flash monitor
```

**Obs.:** Mude o argumento `/dev/ttyUSB0` para o caminho do dispositivo USB ao qual a placa está conectada.

O terminal começará a printar os logs enviados pela placa conforme ela executa o código.


### Requisitos atendendidos e não atendendidos

O servidor central mostra o estado dos dispositivos, com as informações de temperatura, umidade e saída. O estado de entrada não é exibido, mas é capturado pelo servidor, que por sua vez é usado para o mecanismo de acionamento de alarme. O servidor central também permite a escolha do tipo de saída (dimerizável ou liga/desliga) e provê o acionamento destes dispositivos. A geração do arquivo CSV não foi implementada.

A esp Bateria não foi implementada.

A esp Energia teve todos os requisitos atendidos, entretanto pode apresentar alguns problemas de reinicialização em casos especifícos do envio do servidor central. O correto funcionamento da esp pode ser verificado utilizando a ferramenta MQTTBox. A comunicação acordada para o uso do Broker, com os tópicos e respectivos formatos de json, encontram-se no arquivo [comm.md](./comm.md).

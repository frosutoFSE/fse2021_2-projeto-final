#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "dht_wrapper.h"
#include "dht.h"
#include "mqtt_msg.h"

#define DHT_PIN GPIO_NUM_4
#define MAX_ERR_COUNTER 10
#define VECTOR_LEN 5

char* service_temperature = "temperatura";
char* service_humidity = "umidade";

float get_average(float* v, int n) {
    float sum = 0;
    for (int i = 0; i < n; i++) {
        sum += v[i];
    }
    return sum / n;
}

void set_zero_vector(float* v, int n) {
    for (int i = 0; i < n; i++) {
        v[i] = 0.0;
    }
}

void dht_read_loop(void *params) {
    float temperature[VECTOR_LEN], humidity[VECTOR_LEN];
    set_zero_vector(temperature, VECTOR_LEN);
    set_zero_vector(humidity, VECTOR_LEN);

    int8_t counter;
    int8_t err_counter = 0;
    int max_reached = false;

    // Por algum motivo a primeira leitura geralmente dá erro, então ignoremos ela...
    dht_read_float_data(DHT_TYPE_DHT11, DHT_PIN, NULL, NULL);

    for (counter = -1;; counter = (counter+1) % 5) {
        if (dht_read_float_data(DHT_TYPE_DHT11, DHT_PIN, &humidity[counter], &temperature[counter]) == ESP_OK) {
//             ESP_LOGI("DHT", "Umidade: %.1f%%  Temperatura: %.1f", humidity[counter], temperature[counter]);
            err_counter = 0;
            max_reached = false;
        } else {
            err_counter = err_counter < MAX_ERR_COUNTER ? err_counter+1 : MAX_ERR_COUNTER;
            if (err_counter == MAX_ERR_COUNTER && !max_reached) {
                // Se chegar ao número máximo de erros, os vetores são zerados
                // e o único valor que chegará no servidor central será 0.
                set_zero_vector(temperature, VECTOR_LEN);
                set_zero_vector(temperature, VECTOR_LEN);
                max_reached = true;
            }
            // Neste caso, se não ocorrerem muitas falhas seguidas,
            // a última leitura correta ainda é uma boa estimativa :)
            humidity[counter] = humidity[(counter+VECTOR_LEN-1)%VECTOR_LEN];
            temperature[counter] = temperature[(counter+VECTOR_LEN-1)%VECTOR_LEN];
        }

        vTaskDelay(2000 / portTICK_PERIOD_MS);
        if (counter == 4) {
            float temp_avg = get_average(temperature, VECTOR_LEN);
            float hum_avg = get_average(humidity, VECTOR_LEN);
            ESP_LOGI("DHT_AVG", "Umidade: %.1f%%  Temperatura: %.1f", hum_avg, temp_avg);

            mqtt_msg* msg = mqtt_msg_create(MQTT_SEND_FLOAT, service_temperature, NULL, &temp_avg, NULL);
            handle_and_delete(msg);

            msg = mqtt_msg_create(MQTT_SEND_FLOAT, service_humidity, NULL, &hum_avg, NULL);
            handle_and_delete(msg);
        }
    }
}

void dht_wrapper_setup() {
    gpio_config_t conf = {
        .intr_type = GPIO_INTR_DISABLE,
        .mode = GPIO_MODE_OUTPUT,
        .pin_bit_mask = (1ULL << DHT_PIN),
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_ENABLE
    };

    gpio_config(&conf);

    xTaskCreate(dht_read_loop, "DHT_READ", 4096, NULL, 2, NULL);
}

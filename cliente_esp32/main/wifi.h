#ifndef _WIFI_H_
#define _WIFI_H_

#include "esp_system.h"

void start_wifi();
void stop_wifi();

#endif
#ifndef ESP_INFO_H
#define ESP_INFO_H

#include <stdint.h>

#define MAC_STRING_LEN 18
#define CONFIG_TOPIC_LEN 50
#define SPECIFIC_TOPIC_LEN 120

#define MATRICULA CONFIG_MATRICULA

#if defined(CONFIG_ESP_ENERGIA)
#define ESP_TYPE "Energia"
#elif defined(CONFIG_ESP_BATERIA)
#define ESP_TYPE "Bateria"
#endif

typedef struct EspIO_Info {
    char nome_dispositivo[30];
} EspIO_Info;

typedef struct EspInfo {
    int conected;
    int8_t dimmer;
    char tipo[8];
    char general_name[6];
    char mac_address[MAC_STRING_LEN];
    char topic_prefix[8];
    char matricula[10];
    char comodo[30];
    EspIO_Info* input_info;
    EspIO_Info* output_info;

    char topic_config[CONFIG_TOPIC_LEN];
    char topic[SPECIFIC_TOPIC_LEN];

} EspInfo;

/*
 * Verifica se a esp tem salva as configurações do tópico do comodo
 */
int verify_connection_state();

/*
 * Salva as informações relativas à configuração da conexão
 */
void save_info();

/*
 * Carrega as informações importantes da aplicação. Caso o as informações já estejam carregadas,
 * a função sempre o ponteiro do objeto configurado
 */
EspInfo* create_or_get_esp_info();

/*
 * Adiciona informações para a conexão da esp
 */
void add_connection_info(char* comodo, char* input_name, char* output_name, int dimmer);

/*
 * Salva no buffer o nome do tópico de configuração
 */
void get_config_topic_name(char* buffer);

/*
 * Salva no buffer o nome do tópico do comodo + o serviço da mensagem
 */
void get_specific_topic(char* buffer, char* service);

/*
 * retorna 1 se existe a configuração de conexão, e 0 caso contrário.
 * A função verify_connection_state faz uma leitura na flash, enquanto esta é um acesso direto ao atributo
 */
int has_connection_config();

/*
 * inicializa os serviços da esp
 */
void init_esp_services(int8_t dimmer);

/*
 * desconecta a esp do tópico do comodo e reinicia a esp.
 */
void disconnect_from_central();

void log_esp_info();

#endif

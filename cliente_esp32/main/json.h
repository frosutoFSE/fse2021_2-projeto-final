#ifndef JSON_H
#define JSON_H

char* get_sender(char* json_msg);

void config_init_serialize(char* buffer, const char* esp_type);
void config_finish_connection_serialize(char* buffer);

void config_init_deserialize(const char* json_msg, char* room, int* dimmer, char** input_name, char** output_name);

void config_get_type(const char* json_msg, char* config_type);

void app_serialize_number_state(char* buffer, const char* name, double data);
void app_deserialize_number_state(const char* json_msg, const char* name, double* data_holder);

void app_serialize_io_state(char* buffer, const char* io_type, const char* device_name, int data);
void app_deserialize_io_state(const char* json_msg, char* io_type, char* device_name, int* data);

void app_deserialize_get_sender(const char* json_msg, char* sender);

#endif

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "wifi.h"
// #include "gpio_output.h"
#include "gpio_input.h"
#include "dht_wrapper.h"
#include "driver/gpio.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_handler.h"
#include "esp_info.h"

static void general_setup(void) {
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    gpio_input_setup();

    EspInfo* esp = create_or_get_esp_info();
    if (esp == NULL) {
        ESP_ERROR_CHECK(ESP_FAIL);
    }
    log_esp_info();
}

void app_main(void) {

    general_setup();
    start_wifi();

    mqtt_app_start();

}


// #define DHT GPIO_NUM_4
// void app_main(void) {

    // gpio_config_t conf = {
    //     .intr_type = GPIO_INTR_DISABLE,
    //     .mode = GPIO_MODE_OUTPUT,
    //     .pin_bit_mask = (1ULL << DHT),
    //     .pull_down_en = GPIO_PULLDOWN_DISABLE,
    //     .pull_up_en = GPIO_PULLUP_ENABLE
    // };

    // gpio_config(&conf);

    // float temperature, humidity;

    // while (1) {
    //     int rslt = dht_read_float_data(DHT_TYPE_DHT11, DHT, &humidity, &temperature);
    //     if (rslt == ESP_OK) {
    //         ESP_LOGI("DHT", "Umidade: %.1f%% Temperatura: %.1f", humidity, temperature);
    //     } else {
    //         ESP_LOGI("DHT", "Leitura falhou");
    //     }
    //     vTaskDelay(2000 / portTICK_PERIOD_MS);
    // }

    // esp_err_t ret = nvs_flash_init();
    // if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    //     ESP_ERROR_CHECK(nvs_flash_erase());
    //     ret = nvs_flash_init();
    // }
    // ESP_ERROR_CHECK(ret);
    
    // start_wifi();
    // gpio_output_setup();

    // gpio_input_setup();

    // int i = 0;
    // for (;;i = (i+1)%2) {
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    //     gpio_output_set_level(i);
    // }

    
// }

#include <string.h>
#include "topic_utils.h"
#include "esp_log.h"

void topic_append(char* topic, char* append_name) {
    strcat(topic, "/");
    strcat(topic, append_name);
}

void topic_get_last_name(char* topic, char* buffer) {
    int n = strlen(topic);
    int last_slash_index = 0;
    for (int i = 0; i < n; i++) {
        if (topic[i] == '/') {
            last_slash_index = i;
        }
    }
    if (last_slash_index < n-1) {
        strcpy(buffer, &topic[last_slash_index+1]);
    }
}

// int get_num_topic_parts(char* topic) {
//     int n = strlen(topic);
//     int num_of_parts = 0;
//     for (int i = 0; i < n; i++) {
//         if (topic[i] == '/') {
//             num_of_parts = (i == 0 || i == n-1) ? num_of_parts : num_of_parts+1;
//         }
//     }
//     return num_of_parts;
// }

// int get_part_name(char* topic, )


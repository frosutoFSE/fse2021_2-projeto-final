#ifndef TOPIC_HANDLER_H
#define TOPIC_HANDLER_H

#define TOPIC_MAX_LEN 50

void topic_append(char* topic, char* append_name);
void topic_get_last_name(char* topic, char* buffer);

#endif

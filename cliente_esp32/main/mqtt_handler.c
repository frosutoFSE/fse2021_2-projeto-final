#include "string.h"
#include <stdio.h>
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_mac.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#include "esp_err.h"
#include "esp_log.h"
#include "mqtt_client.h"

#include "json.h"
#include "gpio_output.h"
#include "mqtt_handler.h"
#include "mqtt_msg.h"
#include "esp_info.h"

static const char* TAG = "MQTT_CLIENT";
static esp_mqtt_client_handle_t client;
// static SemaphoreHandle_t mqtt_mutex;

static char config_topic[CONFIG_TOPIC_LEN];

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispached from event loop base=%s, event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
//     char config_topic[50];
    char topic[SPECIFIC_TOPIC_LEN];

    switch((esp_mqtt_event_id_t)event_id) {
        
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT Client connected");

            //Se inscreve no tópico de configuração
            esp_mqtt_client_subscribe(client, config_topic, 1);

            if (has_connection_config()) {
                get_specific_topic(topic, "estado");
                esp_mqtt_client_subscribe(client, topic, 1);
                ESP_LOGI(TAG, "Inscrito no tópico (%s)", topic);
            } else {
                mqtt_msg* msg = mqtt_msg_create(MQTT_SEND_CONN, NULL, NULL, NULL, NULL);
                handle_and_delete(msg);
                ESP_LOGI(TAG, "Enviado pedido de conexão");
            }
            break;

        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "Data received from topic (%s)", event->topic);
//             ESP_LOGI(TAG, "Data received: %s", event->data);

            if (!strcmp(config_topic, event->topic)) {
                // mensagem recebida do tópico de configuração
                mqtt_msg* msg = mqtt_msg_create(MQTT_RECV_CONN, NULL, event->data, NULL, NULL);
                handle_and_delete(msg);

                get_specific_topic(topic, "estado");
                esp_mqtt_client_subscribe(client, topic, 1);
                break;
            }

            get_specific_topic(topic, "estado");
            if (!strcmp(topic, event->topic)) {
                // mensagem recebida do tópico do comodo
                mqtt_msg* msg = mqtt_msg_create(MQTT_RECV_OUTPUT, "estado", event->data, NULL, NULL);
                handle_and_delete(msg);
            }
            break;

        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
                ESP_LOGI(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
            }
            break;

        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
}

void mqtt_app_start(void) {

    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = CONFIG_BROKER_MQTT_ADDR,
        .port = CONFIG_BROKER_MQTT_PORT
    };

    get_config_topic_name(config_topic);
    // mqtt_mutex = xSemaphoreCreateMutex();

    // if (mqtt_mutex == NULL) {
    //     ESP_ERROR_CHECK(ESP_FAIL);
    // }

    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
    esp_mqtt_client_start(client);
}

int mqtt_app_publish(char* topic, char* message, int qos) {
    int msg_id = -1;
    // xSemaphoreTake(mqtt_mutex, portMAX_DELAY);
    msg_id = esp_mqtt_client_publish(client, topic, message, 0, qos, NOT_RETAIN);
    // xSemaphoreGive(mqtt_mutex);
    ESP_LOGI(TAG, "Enviada mensagem no topico (%s)", topic);
    return msg_id;
}

// int mqtt_app_publish_msg(char* topic_sufix, char* message, int qos) {
//     char topic[TOPIC_MAX_LEN];
//     memset(topic, 0, TOPIC_MAX_LEN);
//     get_topic(topic, topic_sufix);
//     return mqtt_app_publish(topic, message, qos);
// }

#include <string.h>
#include <stdlib.h>
#include "esp_log.h"
#include "mqtt_msg.h"
#include "mqtt_handler.h"
#include "esp_info.h"
#include "json.h"
#include "gpio_output.h"
#include "gpio_input.h"
#include "dht_wrapper.h"

#define send_control(send) { if (!send) return;}

mqtt_msg* mqtt_msg_create(int type, char* service, char* msg, float* float_data, int* state_data) {
    mqtt_msg* mmsg = malloc(sizeof(mqtt_msg));

    mmsg->type = type;

    if (service != NULL) {
        mmsg->service_name = service;
    }
    if (msg != NULL) {
        mmsg->msg = msg;
    }
    if (float_data != NULL) {
        mmsg->float_data = *float_data;
    }
    if (state_data != NULL) {
        mmsg->state_data = *state_data;
    }
    return mmsg;
}

void mqtt_msg_delete(mqtt_msg* m) {
    free(m);
}

static void handle_send_connection(mqtt_msg* msg, char* esp_type) {

    char buffer_msg[DATA_BUFFER_LEN];
    char buffer_topic[CONFIG_TOPIC_LEN];

    get_config_topic_name(buffer_topic);

    if (msg->type == MQTT_SEND_CONN) {
        config_init_serialize(buffer_msg, esp_type);
        mqtt_app_publish(buffer_topic, buffer_msg, 1);
        return;
    }

    if (msg->type == MQTT_SEND_DCONN) {
        config_finish_connection_serialize(buffer_msg);
        mqtt_app_publish(buffer_topic, buffer_msg, 1);
        disconnect_from_central();
        return;
    }
}

static void handle_recv_connection(mqtt_msg* msg) {
    char conn_type[11];
    config_get_type(msg->msg, conn_type);

    if (!strcmp(conn_type, "desconexao")) {
        if (!has_connection_config()) return;
        disconnect_from_central();
        return;
    }

    if (!strcmp(conn_type, "conexao")) {
        char room[30];
        int dimmer;
        char* input_name = NULL;
        char* output_name = NULL;

        // extrai informação
        config_init_deserialize(msg->msg, room, &dimmer, &input_name, &output_name);

        //configura informações de conexão da esp_info
        add_connection_info(room, input_name, output_name, dimmer);

        free(input_name);
        free(output_name);
    }
}

static void handle_float_send(mqtt_msg* msg) {
    char buffer[DATA_BUFFER_LEN];
    char buffer_topic[SPECIFIC_TOPIC_LEN];

    get_specific_topic(buffer_topic, msg->service_name);

    app_serialize_number_state(buffer, "dado", msg->float_data);
    mqtt_app_publish(buffer_topic, buffer, 1);
}

static void handle_state_send(mqtt_msg* msg, const char* io_type, const char* device_name) {
    char buffer[DATA_BUFFER_LEN];
    char buffer_topic[SPECIFIC_TOPIC_LEN];

    get_specific_topic(buffer_topic, msg->service_name);
    app_serialize_io_state(buffer, io_type, device_name, msg->state_data);

    mqtt_app_publish(buffer_topic, buffer, 1);
}

static void handle_state_recv(mqtt_msg* msg, char* device_name) {
    char io_type[7];
    char device_name_received[20];
    int state;
    app_deserialize_io_state(msg->msg, io_type, device_name_received, &state);

    //A única mensagem enviada pelo servidor para estado é de output
    if (strcmp(io_type, "output") != 0) return;

    //confirma se a mensagem é realmente para este dispositivo
    if (strcmp(device_name, device_name_received) != 0) return;

    int pin_state = gpio_output_set_level(state);

    // retorna o estado do pino para o servidor
    mqtt_msg* send = mqtt_msg_create(MQTT_SEND_OUTPUT, "estado", NULL, NULL, &pin_state);
    handle_state_send(send, "output", device_name);
    mqtt_msg_delete(send);
}

static int is_same_sender(mqtt_msg* msg, char* device_name) {
    if (msg->type != MQTT_RECV_CONN && msg->type != MQTT_RECV_OUTPUT)
        return 0;

    char sender[10];
    app_deserialize_get_sender(msg->msg, sender);

    if (!strcmp(sender, device_name)) {
        return 1;
    }

    return 0;
}

void mqtt_msg_handle(mqtt_msg* msg) {
    EspInfo* esp_info = create_or_get_esp_info();

    // primeiro olha se a mensagem não é enviada pela própria esp
    if (is_same_sender(msg, esp_info->general_name)) return;

    // se o comodo não está setado, não é pra enviar
    int8_t send = esp_info->comodo[0] == '\0' ? 0 : 1;

    switch (msg->type) {
        case MQTT_SEND_CONN:
            handle_send_connection(msg, esp_info->tipo);
            break;
        case MQTT_SEND_DCONN:
            send_control(send);
            handle_send_connection(msg, esp_info->tipo);
            break;
        case MQTT_RECV_CONN:
            ESP_LOGI("MQTT_MSG", "Recebida mensagem para configuracao.");
            handle_recv_connection(msg);
            break;
        case MQTT_SEND_FLOAT:
            send_control(send);
            handle_float_send(msg);
            break;
        case MQTT_SEND_INPUT:
            send_control(send);
            handle_state_send(msg, "input", esp_info->input_info->nome_dispositivo);
            break;
        case MQTT_SEND_OUTPUT:
            send_control(send);
            handle_state_send(msg, "output", esp_info->output_info->nome_dispositivo);
            break;
        case MQTT_RECV_OUTPUT:
            handle_state_recv(msg, esp_info->output_info->nome_dispositivo);
            break;
    }
}

#ifndef GPIO_INPUT_H
#define GPIO_INPUT_H

void gpio_input_setup();
void gpio_input_start_intr_handler();
void gpio_input_stop_intr_handler();

#endif
#ifndef MQTT_MSG_H
#define MQTT_MSG_H

#define MQTT_SEND_CONN   1
#define MQTT_RECV_CONN   2
#define MQTT_SEND_FLOAT  3
#define MQTT_SEND_INPUT  4
#define MQTT_SEND_OUTPUT 5
#define MQTT_RECV_OUTPUT 6
#define MQTT_SEND_DCONN  7

#define DATA_BUFFER_LEN 200

#define handle_and_delete(msg) { mqtt_msg_handle(msg); mqtt_msg_delete(msg); }

typedef struct mqtt_msg {
    int type;
    // nome do serviço sendo chamado (último item de um tópico)
    char* service_name;
    // mensagem json. Usada principalmente para o contexto de recebimento
    char* msg;
    float float_data;
    int state_data;
} mqtt_msg;

// mqtt_msg* mqtt_msg_create(char* topic, char* msg, int type);

mqtt_msg* mqtt_msg_create(int type, char* service, char* msg, float* float_data, int* state_data);
void mqtt_msg_delete(mqtt_msg*);

void mqtt_msg_handle(mqtt_msg*);

#endif

#ifndef NVS_HANDLER_H
#define NVS_HANDLER_H

#include "nvs.h"
#include <stddef.h>
#include <stdint.h>

int get_int8_value(char* namespace, char* key, int8_t* value);
int set_int8_value(char* namespace, char* key, int8_t value);

int get_str_value(char* namespace, char* key, char* value, size_t* length);
int set_str_value(char* namespace, char* key, char* value);

#endif

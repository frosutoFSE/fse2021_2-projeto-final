#include <string.h>
#include <stdlib.h>
#include "esp_log.h"
#include "cJSON.h"
#include "json.h"
#include "esp_info.h"

static cJSON* create_json_obj() {
    EspInfo* esp_info = create_or_get_esp_info();
    cJSON* obj = cJSON_CreateObject();
    cJSON_AddStringToObject(obj, "sender", esp_info->general_name);
    return obj;
}

static void copy_and_free_src(char* dest, char* src) {
    strcpy(dest, src);
    free(src);
}

void config_init_serialize(char* buffer, const char* esp_type) {
    cJSON* obj = create_json_obj();
    cJSON_AddStringToObject(obj, "config", "conexao");
    cJSON_AddStringToObject(obj, "esp_type", esp_type);
    copy_and_free_src(buffer, cJSON_Print(obj));
    cJSON_Delete(obj);
}

void config_finish_connection_serialize(char* buffer) {
    cJSON* obj = create_json_obj();
    cJSON_AddStringToObject(obj, "config", "desconexao");
    copy_and_free_src(buffer, cJSON_Print(obj));
    cJSON_Delete(obj);
}

static void extract_io_name(cJSON* obj, const char* tipo, char** name) {
    cJSON_bool has_item = cJSON_HasObjectItem(obj, tipo);
    if (has_item) {
        cJSON* item = cJSON_GetObjectItem(obj, tipo);
        cJSON* item_dispositivo = cJSON_GetObjectItem(item, "dispositivo");
        *name = malloc(sizeof(char)*50);
        strcpy(*name, cJSON_GetStringValue(item_dispositivo));
    } else {
        *name = NULL;
    }
}

void config_init_deserialize(const char* json_msg, char* room, int* dimmer, char** input_name, char** output_name) {
    cJSON* obj = cJSON_Parse(json_msg);

    cJSON* item_room = cJSON_GetObjectItem(obj, "comodo");
    strcpy(room, cJSON_GetStringValue(item_room));

    cJSON* item_dimmer = cJSON_GetObjectItem(obj, "dimerizavel");
    *dimmer = (int)cJSON_GetNumberValue(item_dimmer);

    extract_io_name(obj, "input", input_name);
    extract_io_name(obj, "output", output_name);

    cJSON_Delete(obj);
}

void config_get_type(const char* json_msg, char* config_type) {
    cJSON* obj = cJSON_Parse(json_msg);
    cJSON* config_obj = cJSON_GetObjectItem(obj, "config");
    strcpy(config_type, cJSON_GetStringValue(config_obj));
    cJSON_Delete(obj);
}

void app_serialize_number_state(char* buffer, const char* name, double data) {
    cJSON* obj = create_json_obj();
    cJSON_AddNumberToObject(obj, name, data);
    copy_and_free_src(buffer, cJSON_Print(obj));
    cJSON_Delete(obj);
}

void app_serialize_io_state(char* buffer, const char* io_type, const char* device_name, int data) {
    cJSON* obj = create_json_obj();
    cJSON* io_obj = cJSON_AddObjectToObject(obj, io_type);
    cJSON_AddStringToObject(io_obj, "dispositivo", device_name);
    cJSON_AddNumberToObject(io_obj, "estado", data);
    copy_and_free_src(buffer, cJSON_Print(obj));
    cJSON_Delete(obj);
}

void app_deserialize_io_state(const char* json_msg, char* io_type, char* device_name, int* data) {
    cJSON* obj = cJSON_Parse(json_msg);

    cJSON* io_obj;
    if (cJSON_HasObjectItem(obj, "input")) {
        io_obj = cJSON_GetObjectItem(obj, "input");
        strcpy(io_type, "input");
    } else if (cJSON_HasObjectItem(obj, "output")) {
        io_obj = cJSON_GetObjectItem(obj, "output");
        strcpy(io_type, "output");
    } else {
        ESP_LOGI("MSG_HANDLER", "Falha na Deserialização. JSON não apresenta atributo 'input' nem 'output'.");
        return;
    }

    cJSON* device_obj = cJSON_GetObjectItem(io_obj, "dispositivo");
    strcpy(device_name, cJSON_GetStringValue(device_obj));

    cJSON* state_obj = cJSON_GetObjectItem(io_obj, "estado");
    *data = (int)cJSON_GetNumberValue(state_obj);

    cJSON_Delete(obj);
}

void app_deserialize_number_state(const char* json_msg, const char* name, double* data_holder) {
    cJSON* obj = cJSON_Parse(json_msg);
    cJSON* item = cJSON_GetObjectItem(obj, name);
    *data_holder = cJSON_GetNumberValue(item);
    cJSON_Delete(obj);
}

void app_deserialize_get_sender(const char* json_msg, char* sender) {
    cJSON* obj = cJSON_Parse(json_msg);
    cJSON* item = cJSON_GetObjectItem(obj, "sender");
    strcpy(sender, cJSON_GetStringValue(item));
    cJSON_Delete(obj);
}

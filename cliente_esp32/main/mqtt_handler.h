#ifndef MQTT_HANDLER_H
#define MQTT_HANDLER_H

#define QOS_0 0
#define QOS_1 1
#define QOS_2 2

#define RETAIN 1
#define NOT_RETAIN 0

void mqtt_app_start();
int mqtt_app_publish(char* topic, char* message, int qos);
// int mqtt_app_publish_msg(char* topic_sufix, char* message, int qos);

#endif

#include <string.h>
#include <stdint.h>
#include "esp_mac.h"
#include "esp_system.h"
#include "esp_err.h"
#include "esp_log.h"
#include "nvs_handler.h"
#include "topic_utils.h"
#include "esp_info.h"
#include "gpio_output.h"
#include "gpio_input.h"
#include "dht_wrapper.h"


EspInfo* esp_info = NULL;

static int create_io_info() {
    esp_info->input_info = malloc(sizeof(EspIO_Info));
    if (esp_info->input_info == NULL) {
        return ESP_FAIL;
    }
    memset(esp_info->input_info, 0, sizeof(EspIO_Info));

    esp_info->output_info = malloc(sizeof(EspIO_Info));
    if (esp_info->output_info == NULL) {
        free(esp_info->input_info);
        return ESP_FAIL;
    }
    memset(esp_info->output_info, 0, sizeof(EspIO_Info));
    return ESP_OK;
}

static void mac_address_setup() {

    uint8_t mac[18];
    esp_read_mac(mac, ESP_MAC_WIFI_STA);
    sprintf(esp_info->mac_address, "%.2X:%.2X:%.2X:%.2X:%.2X:%.2X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    ESP_LOGI("MAC", "Endereço MAC configurado: %s", esp_info->mac_address);
}

int has_connection_config() {
    return esp_info->conected;
}

int verify_connection_state() {
    int8_t connection_state = 0;
    int rslt = get_int8_value("connection", "broker_conn", &connection_state);

    switch (rslt) {
        case ESP_OK:
            return connection_state;
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            // se o valor não foi achado, então a esp não deve estar conectada a nenhum broker
            set_int8_value("connection", "broker_conn", 0);
            return 0;
            break;
        default:
            ESP_ERROR_CHECK(rslt);
            break;
    }
    return 0;
}

static void set_info_default_values() {
    //     seta o estado inicial correto da struct
    strcpy(esp_info->general_name, "esp32");
    strcpy(esp_info->tipo, ESP_TYPE);
    mac_address_setup();
    strcpy(esp_info->topic_prefix, "fse2021");
    strcpy(esp_info->matricula, MATRICULA);
}

static void load_info() {
    get_int8_value("connection", "dimmer", &esp_info->dimmer);

    size_t len = 30;
    get_str_value("connection", "comodo", esp_info->comodo, &len);

    len = 30;
    get_str_value("connection", "input_name", esp_info->input_info->nome_dispositivo, &len);

    len = 30;
    get_str_value("connection", "output_name", esp_info->output_info->nome_dispositivo, &len);
}

void save_info() {
    set_str_value("connection", "comodo", esp_info->comodo);
    set_str_value("connection", "input_name", esp_info->input_info->nome_dispositivo);
    set_str_value("connection", "output_name", esp_info->output_info->nome_dispositivo);
    set_int8_value("connection", "dimmer", esp_info->dimmer);
    set_int8_value("connection", "broker_conn", 1);
}

EspInfo* create_or_get_esp_info() {

    // a variável é uma singleton
    if (esp_info != NULL) return esp_info;

    esp_info = malloc(sizeof(EspInfo));
    if (esp_info == NULL) return NULL;

    memset(esp_info, 0, sizeof(EspInfo));

    int io_created = create_io_info();
    if (io_created != ESP_OK) {
        free(esp_info);
        return NULL;
    }

    set_info_default_values();

    int conn = verify_connection_state();
    if (conn) {
        esp_info->conected = 1;
        load_info();
        ESP_LOGI("ESP_INFO", "Data loaded from nvs");
        init_esp_services(esp_info->dimmer);
    } else {
        esp_info->conected = 0;
        ESP_LOGI("ESP_INFO", "Data not loaded from nvs");
    }

    char config_topic[50];
    get_config_topic_name(config_topic);
    ESP_LOGI("ESP_INFO", "config topic: %s",config_topic);
    return esp_info;
}

void add_connection_info(char* comodo, char* input_name, char* output_name, int dimmer) {
    strcpy(esp_info->comodo, comodo);

    esp_info->dimmer = dimmer;
    if (input_name != NULL)
        strcpy(esp_info->input_info->nome_dispositivo, input_name);
    if (output_name != NULL)
        strcpy(esp_info->output_info->nome_dispositivo, output_name);

    save_info();
    esp_info->conected = 1;

    log_esp_info();

    init_esp_services(esp_info->dimmer);
}

void init_esp_services(int8_t dimmer) {
    if (dimmer) {
        gpio_output_setup(DIMMER_MODE);
    } else {
        gpio_output_setup(BIN_MODE);
    }
    gpio_output_set_level(0);
//     gpio_input_start_intr_handler();
    dht_wrapper_setup();
}

void get_config_topic_name(char* buffer) {
    if (esp_info->topic_config[0] != '\0') {
        strcpy(buffer, esp_info->topic_config);
        return;
    }
    strcpy(buffer, esp_info->topic_prefix);
    topic_append(buffer, esp_info->matricula);
    topic_append(buffer, "dispositivos");
    topic_append(buffer, esp_info->mac_address);

    strcpy(esp_info->topic_config, buffer);
}

void get_specific_topic(char* buffer, char* service) {
//     if (esp_info->topic[0] != '\0') {
//         strcpy(buffer, esp_info->topic);
//         if (service != NULL)
//             topic_append(buffer, service);
//         return;
//     }

    strcpy(buffer, esp_info->topic_prefix);
    topic_append(buffer, esp_info->matricula);
    topic_append(buffer, esp_info->comodo);

    strcpy(esp_info->topic, buffer);

    if (service != NULL)
        topic_append(buffer, service);
}

void disconnect_from_central() {
    gpio_output_stop();
    gpio_input_stop_intr_handler();
    set_int8_value("connection", "broker_conn", 0);
    esp_restart();
}

void log_esp_info() {
    ESP_LOGI("ESP_INFO", "\nconnected:%d\ndimerizavel:%d\ntipo:%s\ngeneral_name:%s\nmac_address:%s\ntopic_prefix:%s\nmatricula:%s\ncomodo:%s\ninput_name:%s\noutput_name:%s",
             esp_info->conected, esp_info->dimmer, esp_info->tipo, esp_info->general_name,
             esp_info->mac_address, esp_info->topic_prefix, esp_info->matricula,
             esp_info->comodo, esp_info->input_info->nome_dispositivo,
             esp_info->output_info->nome_dispositivo);
}

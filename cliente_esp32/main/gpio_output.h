#ifndef GPIO_OUTPUT_H
#define GPIO_OUTPUT_H

#include "stdint.h"

#define BIN_MODE    0
#define DIMMER_MODE 1

void gpio_output_setup(int8_t mode);
int gpio_output_set_level(int state);
void gpio_output_stop();

#endif

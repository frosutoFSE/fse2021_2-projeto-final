#include <string.h>
#include "esp_log.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs_handler.h"

int get_int8_value(char* namespace, char* key, int8_t* value) {

    nvs_handle_t nvs_handler;

    esp_err_t nvs_reader = nvs_open(namespace, NVS_READONLY, &nvs_handler);
    if (nvs_reader != ESP_OK) {
        ESP_LOGE("NVS", "ERROR: %s", esp_err_to_name(nvs_reader));
        return nvs_reader;
    }

    esp_err_t read_rslt = nvs_get_i8(nvs_handler, key, value);

    nvs_close(nvs_handler);

    return read_rslt;
}

int set_int8_value(char* namespace, char* key, int8_t value) {

    nvs_handle_t nvs_handler;

    esp_err_t nvs_writer = nvs_open(namespace, NVS_READWRITE, &nvs_handler);
    if (nvs_writer != ESP_OK) {
        ESP_LOGE("NVS", "ERROR: %s", esp_err_to_name(nvs_writer));
        return nvs_writer;
    }

    esp_err_t write_rslt = nvs_set_i8(nvs_handler, key, value);

    if (write_rslt != ESP_OK) {
        ESP_LOGE("NVS_int8", "Erro ao escrever dado: %s", esp_err_to_name(write_rslt));
    }

    esp_err_t commit_rslt = nvs_commit(nvs_handler);

    if (commit_rslt != ESP_OK) {
        ESP_LOGE("NVS_int8", "Erro ao commitar escrita: %s", esp_err_to_name(commit_rslt));
    }

    nvs_close(nvs_handler);
    return write_rslt;
}

int get_str_value(char* namespace, char* key, char* value, size_t* length) {

    nvs_handle_t nvs_handler;

    esp_err_t nvs_reader = nvs_open(namespace, NVS_READONLY, &nvs_handler);
    if (nvs_reader != ESP_OK) {
        ESP_LOGE("NVS_str", "ERROR: %s", esp_err_to_name(nvs_reader));
        return nvs_reader;
    }

    esp_err_t read_rslt = nvs_get_str(nvs_handler, key, value, length);


    if (read_rslt != ESP_OK) {
        ESP_LOGE("NVS_str", "Erro ao ler dado: %s", esp_err_to_name(read_rslt));

        if (read_rslt == ESP_ERR_NVS_INVALID_LENGTH) {
            ESP_LOGE("NVS_str", "Tamanho necessario para comportar string: %zu", *length);
        }
    }

    nvs_close(nvs_handler);
    return read_rslt;
}

int set_str_value(char* namespace, char* key, char* value) {

    nvs_handle_t nvs_handler;

    esp_err_t nvs_writer = nvs_open(namespace, NVS_READWRITE, &nvs_handler);
    if (nvs_writer != ESP_OK) {
        ESP_LOGE("NVS_str", "ERROR: %s", esp_err_to_name(nvs_writer));
        return nvs_writer;
    }

    esp_err_t write_rslt = nvs_set_str(nvs_handler, key, value);

    if (write_rslt != ESP_OK) {
        ESP_LOGE("NVS_str", "Erro ao escrever dado: %s", esp_err_to_name(write_rslt));
    }

    esp_err_t commit_rslt = nvs_commit(nvs_handler);

    if (commit_rslt != ESP_OK) {
        ESP_LOGE("NVS_str", "Erro ao commitar escrita: %s", esp_err_to_name(commit_rslt));
    }

    nvs_close(nvs_handler);
    return write_rslt;
}

// void show_keys() {
//
//     nvs_iterator_t it = nvs_entry_find("nvs", "connection", NVS_TYPE_ANY);
//
//     while (it != NULL) {
//         nvs_entry
//     }
//
// }

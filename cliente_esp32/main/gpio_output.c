#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "driver/gpio.h"
#include "esp_err.h"
#include "esp_log.h"
#include "gpio_output.h"
#include "stdint.h"

#define LED GPIO_NUM_2
static int pin_state = 0;

static int led_mode = -1;

static void set_mode(int8_t mode) {
    if (mode == BIN_MODE) {
        ESP_LOGI("GPIO_OUTPUT", "Saída binária");
    } else if (mode == DIMMER_MODE) {
        ESP_LOGI("GPIO_OUTPUT", "Saída dimerizavel");
    }
    led_mode = mode;
}

void gpio_output_setup(int8_t mode) {
    if (mode != BIN_MODE && mode != DIMMER_MODE) {
        return;
    }

    esp_err_t rslt;
    if (mode == BIN_MODE) {
        gpio_config_t conf = {
            .intr_type = GPIO_INTR_DISABLE,
            .mode = GPIO_MODE_OUTPUT,
            .pin_bit_mask = (1ULL << LED),
            .pull_down_en = GPIO_PULLDOWN_ENABLE,
            .pull_up_en = GPIO_PULLUP_DISABLE
        };

        gpio_config(&conf);
        set_mode(mode);
    } else if (mode == DIMMER_MODE) {
        ledc_timer_config_t timer_config = {
            .speed_mode = LEDC_LOW_SPEED_MODE,
            .duty_resolution = LEDC_TIMER_8_BIT,
            .timer_num = LEDC_TIMER_0,
            .freq_hz = 1000,
            .clk_cfg = LEDC_AUTO_CLK
        };
        rslt = ledc_timer_config(&timer_config);
        ESP_LOGI("GPIO_OUT", "ledc_timer_config %s", esp_err_to_name(rslt));

        ledc_channel_config_t channel_config = {
            .gpio_num = LED,
            .speed_mode = LEDC_LOW_SPEED_MODE,
            .channel = LEDC_CHANNEL_0,
            .timer_sel = LEDC_TIMER_0,
            .duty = 0,
            .hpoint = 0
        };
        rslt = ledc_channel_config(&channel_config);
        ESP_LOGI("GPIO_OUT", "ledc_channel_config %s", esp_err_to_name(rslt));
        set_mode(mode);
    }
    ESP_LOGI("GPIO_OUT", "GPIO set to %s", led_mode == DIMMER_MODE ? "DIMMER" : "BIN");
}

static int force_output_state_to_range(int state) {
    if (state < 0) {
        return 0;
    }
    if (led_mode == DIMMER_MODE && state > 100) {
        return 100;
    }
    if (led_mode == BIN_MODE && state > 1) {
        return 1;
    }
    return state;
}

int gpio_output_set_level(int state) {
    if (led_mode == -1) {
        ESP_LOGI("GPIO_OUT", "Modo de output não setado");
        return pin_state;
    }

    state = force_output_state_to_range(state);

    if (led_mode == BIN_MODE) {
        esp_err_t err = gpio_set_level(LED, state);
        if (err == ESP_OK) {
            pin_state = state;
        }
        ESP_LOGI("GPIO_OUT", "Estado mudado para %d", pin_state);
        return pin_state;
    }
    if (led_mode == DIMMER_MODE) {
        esp_err_t rslt;
        rslt = ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, (int)(state*255/100));
        if (rslt != ESP_OK) return pin_state;
        rslt = ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
        if (rslt != ESP_OK) return pin_state;
        pin_state = state;
        ESP_LOGI("GPIO_OUT", "Estado mudado para %d", pin_state);
        return pin_state;
    }
    return pin_state;
}

void gpio_output_stop() {
    if (led_mode == -1) {
        return;
    }

    if (led_mode == BIN_MODE) {
        gpio_set_level(LED, 0);
    }

    if (led_mode == DIMMER_MODE) {
        ledc_stop(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, 0);
    }
}

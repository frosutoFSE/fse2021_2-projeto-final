#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "esp_err.h"
#include "esp_log.h"
#include "json.h"
#include "mqtt_msg.h"
#include "esp_info.h"
#include "gpio_input.h"

#define BOTAO GPIO_NUM_0

static int input_state = 0;
static QueueHandle_t intr_queue;

static void IRAM_ATTR gpio_isr_handler(void *args) {
    int pin = (int)args;
    xQueueSendFromISR(intr_queue, &pin, NULL);
}

void button_intr_handler(void *params) {
    int pin;
    int counter;
    mqtt_msg* msg = mqtt_msg_create(MQTT_SEND_INPUT, "estado", NULL, NULL, NULL);
    for (;;) {
        counter = 0;
        if (xQueueReceive(intr_queue, &pin, portMAX_DELAY)) {
            while (!gpio_get_level(BOTAO)) {
                vTaskDelay(100 / portTICK_PERIOD_MS);
                counter += 100;
//                 ESP_LOGI("GPIO_INPUT", "Botão pressionado por %d", counter);
                if (counter >= 3000) {
                    ESP_LOGI("GPIO_INPUT", "Botão pressionado por 3 segundos. Reiniciando ESP.");
                    msg->type = MQTT_SEND_DCONN;
                    mqtt_msg_handle(msg);
                }
            }


            input_state = (input_state+1)%2;
            ESP_LOGI("GPIO_INPUT", "Estado modificado para %d", input_state);
            // ESP_LOGI("GPIO_TESTE", "input_state: %d", input_state);
            msg->state_data = input_state;
            mqtt_msg_handle(msg);
        }
    }
}

void gpio_input_setup() {
    gpio_config_t conf = {
        .intr_type = GPIO_INTR_NEGEDGE,
        .mode = GPIO_MODE_INPUT,
        .pin_bit_mask = (1ULL << BOTAO),
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_ENABLE
    };

    gpio_config(&conf);

    intr_queue = xQueueCreate(10, sizeof(int));
    xTaskCreate(button_intr_handler, "GPIO_INPUT", 4096, NULL, 1, NULL);

    gpio_install_isr_service(0);
    gpio_input_start_intr_handler();
}

void gpio_input_start_intr_handler() {
    gpio_isr_handler_add(BOTAO, gpio_isr_handler, (void*) BOTAO);
}

void gpio_input_stop_intr_handler() {
    gpio_isr_handler_remove(BOTAO);
}
